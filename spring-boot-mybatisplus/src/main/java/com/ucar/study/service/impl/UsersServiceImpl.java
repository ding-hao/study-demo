package com.ucar.study.service.impl;

import com.ucar.study.entity.Users;
import com.ucar.study.mapper.UsersDao;
import com.ucar.study.service.MPUsersService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dinghao
 * @since 2018-07-30
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersDao, Users> implements MPUsersService {

}
