package com.ucar.study.service;

import com.ucar.study.entity.Users;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dinghao
 * @since 2018-07-30
 */
public interface MPUsersService extends IService<Users> {

}
