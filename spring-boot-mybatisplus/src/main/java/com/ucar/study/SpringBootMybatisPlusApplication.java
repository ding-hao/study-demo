package com.ucar.study;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMybatisPlusApplication {
    private static final Logger logger = LoggerFactory.getLogger(SpringBootMybatisPlusApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMybatisPlusApplication.class, args);
        logger.info("========================启动完毕========================");
    }
}
