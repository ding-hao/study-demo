package com.ucar.study.web;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.ucar.study.entity.Users;
import com.ucar.study.mapper.UsersDao;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dinghao
 * @since 2018-07-30
 */
@RestController
//@RequestMapping("/users")
@Api(tags="springboot-mybatis-plus")
public class UsersController {

	@Autowired
	private UsersDao usersDao;
	
	@GetMapping("/getUsers")
	@ApiOperation("获取所有用户")
	public List<Users> getUsers() {
		List<Users> users=usersDao.selectList(new EntityWrapper<Users>());
		return users;
	}
	
	@GetMapping("/getUser/{id}")
	@ApiOperation("按id查询用户")
    public Users getUser(@PathVariable("id")@ApiParam("用户id") Long id) {
		Users user=usersDao.selectById(id);
        return user;
    }
    
	@PostMapping("/add")
	@ApiOperation("保存用户")
    public void save(Users user) {
    	usersDao.insert(user);
    }
    
	@PostMapping(value="update")
	@ApiOperation("更新用户")
    public void update(Users user) {
    	usersDao.updateAllColumnById(user);
    }
    
	@GetMapping(value="/delete/{id}")
	@ApiOperation("通过id删除用户")
    public void delete(@PathVariable("id")@ApiParam("用户id") Long id) {
    	usersDao.deleteById(id);
    }
    
    
	
}
