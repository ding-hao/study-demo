package com.ucar.study.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.ucar.study.entity.Users;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dinghao
 * @since 2018-07-30
 */
@Mapper
public interface UsersDao extends BaseMapper<Users> {

}
