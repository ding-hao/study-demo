package com.ucar.study.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.ucar.study.userenum.UserSexEnum;

import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author dinghao
 * @since 2018-07-30
 */
public class Users extends Model<Users> {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(hidden = true)
	private Long id;

	/**
	 * 用户名value–字段说明 name–重写属性名字 dataType–重写属性类型 required–是否必填 example–举例说明
	 * hidden–隐藏
	 */
	@ApiModelProperty(value = "用户名", required = true, example = "panghu")
	private String userName;

	/**
	 * 密码
	 */
	@ApiModelProperty(value = "密码", required = true, example = "123")
	private String passWord;

	@ApiModelProperty(value = "性别", required = true, example = "MEN")
	private UserSexEnum user_sex;

	@ApiModelProperty(value = "别名", example = "panghu")
	private String nick_name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public UserSexEnum getUser_sex() {
		return user_sex;
	}

	public void setUser_sex(UserSexEnum user_sex) {
		this.user_sex = user_sex;
	}

	public String getNick_name() {
		return nick_name;
	}

	public void setNick_name(String nick_name) {
		this.nick_name = nick_name;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Users{" + "id=" + id + ", userName=" + userName + ", passWord=" + passWord + ", user_sex=" + user_sex
				+ ", nick_name=" + nick_name + "}";
	}
}
