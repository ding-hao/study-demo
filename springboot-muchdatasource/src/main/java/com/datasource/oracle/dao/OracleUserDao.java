package com.datasource.oracle.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.datasource.oracle.entity.OracleUser;


public interface OracleUserDao extends JpaRepository<OracleUser,String>,JpaSpecificationExecutor<OracleUser> {


}
