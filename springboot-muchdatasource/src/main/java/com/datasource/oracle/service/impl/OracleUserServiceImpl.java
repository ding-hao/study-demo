package com.datasource.oracle.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datasource.oracle.dao.OracleUserDao;
import com.datasource.oracle.entity.OracleUser;
import com.datasource.oracle.service.OracleUserService;


@Service
public class OracleUserServiceImpl implements OracleUserService{

	@Autowired
	private OracleUserDao userDao;
	
	@Override
	public OracleUser sava(OracleUser user) {
		return userDao.save(user);
	}

}
