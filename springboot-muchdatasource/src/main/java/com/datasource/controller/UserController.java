package com.datasource.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.datasource.mysql.entity.MysqlUser;
import com.datasource.mysql.service.MysqlUserService;
import com.datasource.oracle.entity.OracleUser;
import com.datasource.oracle.service.OracleUserService;


@RestController
public class UserController {

	@Autowired
	private MysqlUserService mysqlUserService;
	
	@Autowired
	private OracleUserService oracleUserService;
	
	@PostMapping("/oracles/save")
	@ResponseBody
	public OracleUser oracleSave(@RequestBody @Validated OracleUser user) {
		return oracleUserService.sava(user);
	}
	
	@GetMapping("/mysql/{id}")
	public MysqlUser mysqlFind(@PathVariable String id) {
		return mysqlUserService.find(id);
	}
}
