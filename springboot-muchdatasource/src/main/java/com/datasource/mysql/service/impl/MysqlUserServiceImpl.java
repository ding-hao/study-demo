package com.datasource.mysql.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datasource.mysql.dao.MysqlUserDao;
import com.datasource.mysql.entity.MysqlUser;
import com.datasource.mysql.service.MysqlUserService;


@Service
public class MysqlUserServiceImpl implements MysqlUserService{

	@Autowired
	private MysqlUserDao userDao;
	
	@Override
	public MysqlUser sava(MysqlUser user) {
		return userDao.save(user);
	}

	@Override
	public MysqlUser find(String id) {
		return userDao.findOne(id);
	}

}
