package com.datasource.mysql.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.datasource.mysql.entity.MysqlUser;

public interface MysqlUserDao extends JpaRepository<MysqlUser,String>,JpaSpecificationExecutor<MysqlUser> {


}
