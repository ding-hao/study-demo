package com.datasource.mysql.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
* @ClassName: User 
* @Description: TODO
* @author dinghao
* @date 2017年12月19日 下午2:20:06 
*
 */
/*
create table TB_USER
(
  id       NUMBER not null,
  username VARCHAR2(100),
  birthday DATE,
  sex      CHAR(2),
  address  VARCHAR2(500)
)
 */

@Entity
@Table(name = "TB_USER")
@ApiModel(value = "用户信息", description = "用户信息")
public class MysqlUser implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid2")
	@ApiModelProperty()
	private String id ;
	
	@ApiModelProperty(value = "姓名")
	@Column(name = "username")
	private String username;
	
	@ApiModelProperty(value = "生日")
	@Column(name = "birthday")
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX")
	@DateTimeFormat
	private Date birthday;
	
	@ApiModelProperty(value = "性别")
	@Column(name = "sex")
	private String sex;
	
	@ApiModelProperty(value = "地址")
	@Column(name = "address")
	private String address;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

//	@Override
//	public String toString() {
//		return "User [id=" + id + ", username=" + username + ", birthday=" + birthday + ", sex=" + sex + ", address="
//				+ address + "]";
//	}

}
