package com.datasource.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//标记配置类@EnableSwagger2 
//开启在线接口文档
@Configuration
@EnableSwagger2
public class Swagger2Config {    
	/**     * 添加摘要信息(Docket)     */    
	@SuppressWarnings("deprecation")
	@Bean    
	public Docket controllerApi() {        
		return new Docket(DocumentationType.SWAGGER_2)                
				.apiInfo(new ApiInfoBuilder()                        
						.title("标题：多数据源的数据库管理系统_接口文档")                        
						.description("描述：多数据源的数据库管理系统...") 
						.contact("小逗比")
						.version("版本号:1.0")                        
						.build())                
				.select()                
				.apis(RequestHandlerSelectors.basePackage("com.datasource.controller"))                
				.paths(PathSelectors.any())                
				.build();    
	}
}
