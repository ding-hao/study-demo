package com.example.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Tools {
	
	
    private static List<SimpleDateFormat> dateFormats = new ArrayList(12) {
        private static final long serialVersionUID = 2249396579858199535L;

        {
            this.add(new SimpleDateFormat("yyyy-MM-dd"));
            this.add(new SimpleDateFormat("yyyy/MM/dd"));
            this.add(new SimpleDateFormat("yyyy.MM.dd"));
            this.add(new SimpleDateFormat("yyyy-MM-dd HH:24:mm:ss"));
            this.add(new SimpleDateFormat("yyyy/MM/dd HH:24:mm:ss"));
            this.add(new SimpleDateFormat("yyyy.MM.dd HH:24:mm:ss"));
            this.add(new SimpleDateFormat("M/dd/yyyy"));
            this.add(new SimpleDateFormat("dd.M.yyyy"));
            this.add(new SimpleDateFormat("M/dd/yyyy hh:mm:ss a"));
            this.add(new SimpleDateFormat("dd.M.yyyy hh:mm:ss a"));
            this.add(new SimpleDateFormat("dd.MMM.yyyy"));
            this.add(new SimpleDateFormat("dd-MMM-yyyy"));
        }
    };
    
    
    public static Date convertToDate(String input) {
        Date date = null;
        if(null == input) {
            return null;
        } else {
            Iterator var2 = dateFormats.iterator();

            while(var2.hasNext()) {
                SimpleDateFormat format = (SimpleDateFormat)var2.next();

                try {
                    format.setLenient(false);
                    date = format.parse(input);
                } catch (ParseException var5) {
                    ;
                }

                if(date != null) {
                    break;
                }
            }

            return date;
        }
    }

    public static String dateToString(Date input) {
        String dateString = null;
        if(null == input) {
            return null;
        } else {
            Iterator var2 = dateFormats.iterator();

            while(var2.hasNext()) {
                SimpleDateFormat format = (SimpleDateFormat)var2.next();

                try {
                    format.setLenient(false);
                    dateString = format.format(input);
                } catch (Exception var5) {
                    ;
                }

                if(dateString != null) {
                    break;
                }
            }

            return dateString;
        }
    }
	/**
	 * 复制文件
	 * 
	 * @param source
	 * @param dest
	 * @throws IOException
	 */
	public static boolean copyFile(File source, File dest) {

		// 保证创建一个新文件
		if (!dest.getParentFile().exists()) { // 如果父目录不存在，创建父目录
			dest.getParentFile().mkdirs();
		}
		if (dest.exists()) { // 如果已存在,删除旧文件
			dest.delete();
		}
		InputStream input = null;
		OutputStream output = null;
		try {
			dest.createNewFile();// 创建文件
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > -1) {
				output.write(buf, 0, bytesRead);
			}
			output.close();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 将InputStream写入本地文件
	 * 
	 * @param filePath
	 *            写入本地目录
	 * @param input
	 *            输入流
	 * @throws IOException
	 */
	public static int writeToLocal(String filePath, InputStream input) {
		// 定义每次读取的长度
		int index = -1;
		// 定义每次读取字节的大小与保存字节的数据
		byte[] bytes = new byte[1024];
		FileOutputStream downloadFile;
		try {
			// 保证创建一个新文件
			File file = new File(filePath);
			if (!file.getParentFile().exists()) { // 如果父目录不存在，创建父目录
				file.getParentFile().mkdirs();
			}
			file.createNewFile();

			downloadFile = new FileOutputStream(filePath);
			while ((index = input.read(bytes)) != -1) {
				downloadFile.write(bytes, 0, index);
				downloadFile.flush();
			}
			downloadFile.close();
			input.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return 0;
		} catch (IOException e) {
			e.printStackTrace();
			return 0;
		}
		return 1;
	}

}
