package com.example.controller;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.example.utils.Tools;

@Controller
public class WangEditorController {
	
	@RequestMapping("/")
	private String showPage() {
		return "wangeditor";
	}

	/**
	 * 图片上传
	 * 
	 * @param request
	 * @param file
	 * @return
	 */
	@RequestMapping("/test/upload")
	@ResponseBody
	public JSONObject upload(HttpServletRequest request, MultipartFile file) {
		JSONObject imgPathObject = new JSONObject();
		Map map = new HashMap();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		List<FileItem> list = null;
		if (isMultipart) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setHeaderEncoding("UTF-8");
			try {
				// 获取文件名（带后缀名）
				String fileName = file.getOriginalFilename();
				String suffixName = fileName.substring(fileName.lastIndexOf("."));
				// 企业新闻id
				String entNewsImgId = UUID.randomUUID().toString();
				fileName = entNewsImgId + suffixName;
				// 获取文件输入流
				InputStream input = file.getInputStream();
				// 获取当前时间
				String StrDateTime = Tools.dateToString(new Date());
				// 获取工程路径
//				String serverAddress = request.getServletContext().getRealPath("/");
//				String entNewsImgPath = serverAddress + "tempRes/busiPic/" + StrDateTime + "/" + fileName;
//				String entNewsImgPath = "tempImagePath/"+StrDateTime+"/"+fileName;
				String entNewsImgPath = " D:/upload/"+fileName;
				System.out.println("entNewsImgPath:" + entNewsImgPath);
				int result = Tools.writeToLocal(entNewsImgPath, input);
				String imgPath = "images/" + fileName;
				System.out.println("imgPath:" + imgPath);
				if (result == 1) {
					map.put("data", imgPath);
					String entNewsStr = JSONObject.toJSONString(map);
					System.out.println(entNewsStr);
					imgPathObject = JSONObject.parseObject(entNewsStr);
					System.out.println(imgPathObject);
				}
			} catch (Exception e) {
			}
		}

		return imgPathObject;
	}

	/**
	 * 获取富文本内容
	 * 
	 * @param request
	 * @param file
	 * @return
	 */
	@RequestMapping("/test/addNews")
	@ResponseBody
	public JSONObject addNews(HttpServletRequest request, MultipartFile file) {
		Map map = new HashMap();
		// 新闻的UUID
		String entNewsId = UUID.randomUUID().toString();
		String newsCon = "";// 新的新闻内容
		String newsImgPath = "";// 新闻图片路径
		String newsContent = request.getParameter("news");// 获取新闻内容
		System.out.println(newsContent);
		// 截取图片路径
		String tempSrc = "img src=\"";
		String[] imgStr = newsContent.split(tempSrc);
		System.out.println(imgStr);
		String[] imgPathStr = new String[imgStr.length];// 图片路径数组
		System.out.println(imgPathStr);
		System.out.println(imgStr.length);
		if (imgStr.length > 1) {
			String[] imgLengthStr = imgStr[1].split("\"");
			
			int imgLength = imgLengthStr[0].length();

			for (int i = 1; i < imgStr.length; i++) {
				newsImgPath = imgStr[i].substring(0, imgLength);
				System.out.println(newsImgPath);
				// 改变图片路径
				String tempPort = "8080/";
				String tempImgPath = request.getServletContext().getRealPath("/") + newsImgPath.split(tempPort)[1];
				String tempImgUUID = newsImgPath.substring(newsImgPath.lastIndexOf("/") + 1);
				System.out.println(tempImgPath);
				String imgPathNewAbove = request.getServletContext().getRealPath("/");
				String imgPathNewBehind = "busiPic/entNewsPic/" + entNewsId + "/pic_" + tempImgUUID;
				String imgPathNew = imgPathNewAbove + imgPathNewBehind;
				System.out.println("imgPathNew:" + imgPathNew);
				File oldFile = new File(tempImgPath);
				File newFile = new File(imgPathNew);
				Boolean bln = Tools.copyFile(oldFile, newFile);
				if (bln)
					imgPathStr[i - 1] = newsImgPath.split(tempPort)[0] + tempPort + imgPathNewBehind;
			}

			newsCon = imgStr[0];
			for (int i = 1; i < imgStr.length; i++) {
				newsCon += tempSrc + imgPathStr[i - 1] + imgStr[i].substring(imgLength);
			}
			System.out.print(newsCon);
			map.put("newsContent", newsCon);

		} else {
			map.put("newsContent", newsContent);
		}
		String newContentStr = JSONObject.toJSONString(map);
		JSONObject result = JSONObject.parseObject(newContentStr);
		return result;
	}
}
