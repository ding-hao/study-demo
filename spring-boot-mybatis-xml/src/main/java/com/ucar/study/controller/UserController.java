package com.ucar.study.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ucar.study.entity.UserEntity;
import com.ucar.study.mapper.UserMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags = "springboot-mybatis-xml")
public class UserController {
	
	@Autowired
	private UserMapper userMapper;
	
	@GetMapping("/getUsers")
	@ApiOperation("获取所有用户")
	public List<UserEntity> getUsers() {
		List<UserEntity> users=userMapper.getAll();
		return users;
	}
	
	@GetMapping("/getUser/{id}")
	@ApiOperation("按id查询用户")
    public UserEntity getUser(@PathVariable("id")@ApiParam("用户id") Long id) {
    	UserEntity user=userMapper.getOne(id);
        return user;
    }
    
	@PostMapping("/add")
	@ApiOperation("保存用户")
    public void save(UserEntity user) {
    	userMapper.insert(user);
    }
    
	@PostMapping(value="update")
	@ApiOperation("更新用户")
    public void update(UserEntity user) {
    	userMapper.update(user);
    }
    
	@GetMapping(value="/delete/{id}")
	@ApiOperation("通过id删除用户")
    public void delete(@PathVariable("id")@ApiParam("用户id") Long id) {
    	userMapper.delete(id);
    }
    
    
}