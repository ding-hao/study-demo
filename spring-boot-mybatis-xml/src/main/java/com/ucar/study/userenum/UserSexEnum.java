package com.ucar.study.userenum;

public enum UserSexEnum {

	MEN("男",1), WOMEN("女",0);
	
	private String sex;
	private Integer index;
	
	UserSexEnum(String sex,Integer index) {
		this.sex = sex;
		this.index = index;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
	
	
}
