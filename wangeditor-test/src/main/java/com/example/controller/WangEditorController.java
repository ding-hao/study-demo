package com.example.controller;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.properties.FileProperties;
import com.example.utils.FileOperateUtil;

@Controller
public class WangEditorController {
	
	@Autowired
	private FileProperties fileProperties;

	private static String top = "<!DOCTYPE html>\r\n" + "<html>\r\n" + "	<head>\r\n"
			+ "		<meta charset=\"UTF-8\">\r\n" + "		<title></title>\r\n" + "	</head>\r\n" + "	<body>";

	private static String end = "	</body>\r\n" + "</html>";

	@RequestMapping("/")
	private String showPage() {
		return "wangeditor";
	}

	@RequestMapping(value = "/test/upload", method = RequestMethod.POST)
	private String returnHTML(HttpServletRequest request) throws IOException {
		String html = top + request.getParameter("html") + end;// 获取贴

		System.out.println(html);
		String fileName = UUID.randomUUID().toString();
		System.out.println(fileName);

		FileOperateUtil.getInstance().writeFileToService(html.getBytes(), fileProperties.getPath(), fileName + ".html");
		request.setAttribute("html", request.getParameter("html"));
		return "wangeditor";
	}

}
